import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import ShowNavbar from "./showNavbar";
import auth from "./authService";
import "./login.css";
import Home from "./home";

class GaanaComp extends Component {
  render() {
    const user = auth.getUser();
    return (
      <React.Fragment>
        <ShowNavbar user={user} />
        <Switch>
          <Route path="/home" component={Home} />
          <Redirect from="/" to="/home" />
        </Switch>
      </React.Fragment>
    );
  }
}
export default GaanaComp;
