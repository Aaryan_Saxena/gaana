import React, { Component } from "react";
import "./login.css";
class Login extends Component {
  render() {
    return (
      <div className="container">
        <div className="login-box">
          <div className="row">
            <div align="right" className="col-12 times-icon">
              <svg width="17" height="17" viewBox="0 0 17 17">
                <path
                  className=""
                  fill-rule="evenodd"
                  d="M16.293 1.592l-1.3-1.3-6.7 6.701-6.7-6.7-1.3 1.299 6.7 6.7-6.7 6.701 1.3 1.3 6.7-6.7 6.7 6.7 1.3-1.3-6.7-6.7z"
                ></path>{" "}
              </svg>
            </div>
            <div className="col-12 text-center">
              <h2 className="second-row">INDIA'S NO. 1 MUSIC APP</h2>
              <h6 className="second-para">
                Over 45 million songs to suit every mood and occasion
              </h6>
            </div>
            <div className="col-12 pt-4 px-0">
              <div className="row text-center px-0">
                <div className="col-2"></div>
                <div className="col-2">
                  <div>
                    {" "}
                    <svg width="30" height="30" viewBox="0 0 30 30">
                      <g fill="none" fill-rule="evenodd">
                        <circle
                          cx="15"
                          cy="15"
                          r="14"
                          stroke="#E72C30"
                          stroke-width="2"
                        ></circle>
                        <path
                          fill="#E72C30"
                          d="M20.132 13.263A.602.602 0 0 1 21 13.8v4.5a1.5 1.5 0 1 1-1.2-1.47v-2.059l-2.4 1.2v3.53a1.5 1.5 0 1 1-1.2-1.47V15.6a.6.6 0 0 1 .332-.538zM14.4 16.2v1.2H9v-1.2h5.4zm0-2.4V15H9v-1.2h5.4zm4.8-2.4v1.2H9v-1.2h10.2zm0-2.4v1.2H9V9h10.2z"
                        ></path>
                      </g>
                    </svg>
                    <div className="icon-heading">
                      Create your <br /> own Playlists
                    </div>
                  </div>
                </div>
                <div className="col-2 offset-1 p-0">
                  <div>
                    {" "}
                    <svg width="30" height="30" viewBox="0 0 30 30">
                      {" "}
                      <g fill="none" fill-rule="evenodd">
                        {" "}
                        <circle
                          cx="15"
                          cy="15"
                          r="14"
                          stroke="#E72C30"
                          stroke-width="2"
                        ></circle>{" "}
                        <path
                          fill="#E72C30"
                          d="M18.6 19.8a1.2 1.2 0 1 1 0-2.4 1.2 1.2 0 0 1 0 2.4m-7.2-3.6a1.2 1.2 0 1 1 0-2.4 1.2 1.2 0 0 1 0 2.4m7.2-6a1.2 1.2 0 1 1 0 2.4 1.2 1.2 0 0 1 0-2.4m0 6c-.72 0-1.36.325-1.8.83l-3.055-1.528a2.384 2.384 0 0 0 0-1.004l3.055-1.527c.44.504 1.08.829 1.8.829 1.324 0 2.4-1.076 2.4-2.4 0-1.324-1.076-2.4-2.4-2.4a2.402 2.402 0 0 0-2.345 2.902L13.2 13.429a2.385 2.385 0 0 0-1.8-.829A2.402 2.402 0 0 0 9 15c0 1.324 1.076 2.4 2.4 2.4.72 0 1.36-.325 1.8-.83l3.055 1.528A2.402 2.402 0 0 0 18.6 21c1.324 0 2.4-1.076 2.4-2.4 0-1.324-1.076-2.4-2.4-2.4"
                        ></path>{" "}
                      </g>{" "}
                    </svg>
                  </div>
                  <div className="icon-heading">
                    Share music with family and friends
                  </div>
                </div>
                <div className="col-2 offset-1">
                  <div>
                    {" "}
                    <svg width="30" height="30" viewBox="0 0 30 30">
                      <g fill="none" fill-rule="evenodd">
                        <circle
                          cx="15"
                          cy="15"
                          r="14"
                          stroke="#E72C30"
                          stroke-width="2"
                        ></circle>{" "}
                        <path
                          fill="#E72C30"
                          d="M12.611 10.5c-1.194 0-2.167 1.01-2.167 2.25 0 2.522 3.354 5.405 5.056 6.605 1.702-1.2 5.056-4.083 5.056-6.605 0-1.24-.973-2.25-2.167-2.25-1.22 0-2.167 1.21-2.167 2.25 0 .414-.323.75-.722.75-.399 0-.722-.336-.722-.75 0-1.04-.947-2.25-2.167-2.25M15.5 21a.704.704 0 0 1-.383-.114C14.867 20.724 9 16.868 9 12.75 9 10.682 10.62 9 12.611 9c1.151 0 2.218.628 2.889 1.56.67-.932 1.738-1.56 2.889-1.56C20.38 9 22 10.682 22 12.75c0 4.118-5.867 7.974-6.117 8.136A.704.704 0 0 1 15.5 21"
                        ></path>{" "}
                      </g>{" "}
                    </svg>
                  </div>
                  <div className="icon-heading">Save your favourites</div>
                </div>
              </div>
            </div>
            <div className="col-12 px-5 mt-3 mx-3">
              <div className="row bottom-b mx-4"></div>
            </div>
            <div className="col-12 mt-2">
              <div className="row p-0">
                <div className="col-4" style={{ maxWidth: "220px" }}></div>
                <div className="col-5 py-3">
                  <button className="btn number-button py-2">
                    Phone Number
                  </button>
                  <div className="row mt-3">
                    <div className="col-4 pr-0 pl-1">
                      <hr style={{ width: "100%", height: "2px" }} />
                    </div>
                    <div className="col-4 py-2 icon-heading p-0">
                      or continue with
                    </div>
                    <div className="col-4">
                      <hr style={{ width: "100%", height: "2px" }} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12">
              <div className="row">
                <div className="col-4" style={{ maxWidth: "220px" }}></div>
                <div className="col-5">
                  <div className="row text-center">
                    <div className="col-4">
                      <svg viewBox="0 0 50 50" style={{ maxWidth: "55px" }}>
                        {" "}
                        <g fill="none" fill-rule="evenodd">
                          {" "}
                          <circle
                            fill="#4267B2"
                            cx="25"
                            cy="25"
                            r="25"
                          ></circle>
                          <path
                            d="M27.294 24.977V37h-4.943V24.977H20V20.75h2.351v-2.734c0-1.955.923-5.017 4.987-5.017l3.662.015v4.102h-2.657c-.436 0-1.048.219-1.048 1.152v2.486h3.694l-.432 4.222h-3.263z"
                            fill="#FFF"
                          ></path>{" "}
                        </g>{" "}
                      </svg>
                      <div className="social-media mt-2">Facebook</div>
                    </div>
                    <div className="col-4">
                      <svg viewBox="0 0 52 52" style={{ maxWidth: "55px" }}>
                        {" "}
                        <g
                          transform="translate(1 1)"
                          fill="none"
                          fill-rule="evenodd"
                        >
                          {" "}
                          <circle
                            stroke-opacity=".3"
                            stroke="#000"
                            fill="#FFF"
                            cx="25"
                            cy="25"
                            r="25"
                          ></circle>{" "}
                          <path
                            d="M36.52 25.273c0-.851-.076-1.67-.218-2.455H25v4.642h6.458a5.52 5.52 0 01-2.394 3.622v3.01h3.878c2.269-2.088 3.578-5.165 3.578-8.82z"
                            fill="#4285F4"
                          ></path>{" "}
                          <path
                            d="M25 37c3.24 0 5.956-1.075 7.942-2.907l-3.878-3.011c-1.075.72-2.45 1.145-4.064 1.145-3.125 0-5.77-2.11-6.715-4.947h-4.009v3.11A11.995 11.995 0 0025 37z"
                            fill="#34A853"
                          ></path>{" "}
                          <path
                            d="M18.285 27.28A7.213 7.213 0 0117.91 25c0-.79.136-1.56.376-2.28v-3.11h-4.009A11.995 11.995 0 0013 25c0 1.936.464 3.77 1.276 5.39l4.01-3.11z"
                            fill="#FBBC05"
                          ></path>{" "}
                          <path
                            d="M25 17.773c1.762 0 3.344.605 4.587 1.794l3.442-3.442C30.951 14.19 28.235 13 25 13c-4.69 0-8.75 2.69-10.724 6.61l4.01 3.11c.943-2.836 3.589-4.947 6.714-4.947z"
                            fill="#EA4335"
                          ></path>{" "}
                        </g>{" "}
                      </svg>
                      <div className="social-media mt-2">Google</div>
                    </div>
                    <div className="col-4">
                      <svg viewBox="0 0 52 52" style={{ maxWidth: "55px" }}>
                        {" "}
                        <g
                          transform="translate(1 1)"
                          fill="none"
                          fill-rule="evenodd"
                        >
                          {" "}
                          <circle
                            stroke-opacity=".3"
                            stroke="#000"
                            fill="#FFF"
                            cx="25"
                            cy="25"
                            r="25"
                          ></circle>{" "}
                          <g fill="#000" fill-opacity=".4" fill-rule="nonzero">
                            {" "}
                            <path d="M33.16 18H16.84c-.54 0-.84.37-.84.906v.453c0 .103.04.185.12.247l8.68 5.703c.14.082.32.082.44 0l8.64-5.806a.296.296 0 00.12-.247v-.35c0-.535-.3-.906-.84-.906z"></path>{" "}
                            <path d="M33.78 21.397l-8.38 5.6c-.14.082-.22.124-.38.124a.588.588 0 01-.36-.124l-8.44-5.518c-.1-.061-.22 0-.22.124v9.409c0 .556.3.906.84.906h16.32c.54 0 .84-.37.84-.906V21.52c0-.124-.12-.206-.22-.124z"></path>{" "}
                          </g>{" "}
                        </g>{" "}
                      </svg>
                      <div className="social-media mt-2">Email</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Login;
