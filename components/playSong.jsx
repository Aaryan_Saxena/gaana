import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./navbar.css";
import $ from "jquery";
class PlaySong extends Component {
  state = { song: "", quality: "HIGH" };
  componentDidMount() {
    $("#tooltip").hide();
    $("#autoplay").hover(function () {
      $("#tooltip").toggle();
    });
    $("#quality-box").hide();
    $("#quality").on("click", function () {
      if ($("#caret")[0].className.includes("fa-caret-up")) {
        $("#caret").removeClass("fa-caret-up");
        $("#caret").addClass("fa-caret-down quality-color");
        $("#quality").addClass("quality-color");
      } else {
        $("#caret").removeClass("fa-caret-down quality-color");
        $("#quality").removeClass("quality-color");
        $("#caret").addClass("fa-caret-up");
      }
      $("#quality-box").toggle();
    });
    let obj = this;
    $("#HD").mouseover(function () {
      if (obj.state.quality != "HD") $("#HD").addClass("quality-hover");
      $("#hdText").text("Upgrade");
    });
    $("#HD").mouseout(function () {
      $("#hdText").text("HD");
      if (obj.state.quality != "HD") $("#HD").removeClass("quality-hover");
    });
    $("#HIGH").hover(function () {
      if (obj.state.quality != "HIGH") $("#HIGH").toggleClass("quality-hover");
    });
    $("#MEDIUM").hover(function () {
      if (obj.state.quality != "MEDIUM")
        $("#MEDIUM").toggleClass("quality-hover");
    });
    $("#LOW").hover(function () {
      if (obj.state.quality != "LOW") $("#LOW").toggleClass("quality-hover");
    });
    $("#additional-info").hide();
    $("#info").on("click", function () {
      if ($("#show-info")[0].className.includes("fa-ellipsis-h")) {
        $("#show-info").removeClass("fa-ellipsis-h");
        $("#show-info").addClass("fa-chevron-down");
      } else {
        $("#show-info").addClass("fa-ellipsis-h");
        $("#show-info").removeClass("fa-chevron-down");
      }
      $("#additional-info").toggle();
    });
    $("#playlist").hover(function () {
      $("#playlist").toggleClass("hover-info");
    });
    $("#share").hover(function () {
      $("#share").toggleClass("hover-info");
    });
    $("#download").hover(function () {
      $("#download").toggleClass("hover-info");
    });
    $("#similar-songs").hover(function () {
      $("#similar-songs").toggleClass("hover-info");
    });
    $("#info-song").hover(function () {
      $("#info-song").toggleClass("hover-info");
    });
    $("#lyrics").hover(function () {
      $("#lyrics").toggleClass("hover-info");
    });
  }
  handleSongPause = (audio) => {
    audio.pause();
    this.setState({ song: "pause" });
  };
  handleSongPlay = (audio) => {
    audio.play();
    this.setState({ song: "play" });
  };
  handleCurrentQuality = (q) =>
    q === this.state.quality ? "quality-hover" : "";
  handleQuality = (q) => {
    $("#quality-box").hide();
    this.setState({ quality: q });
  };
  render() {
    let { playSong = {}, handleChangeSong } = this.props;
    let { quality } = this.state;
    let audio = document.getElementById("myAudio");
    return (
      <div className="row p-0" style={{ marginTop: "55px" }}>
        <div className="col-3 border song-detail">
          <div className="row py-2">
            <div className="col-2">
              <img
                src={playSong.img}
                alt=""
                className="border song-detail-img"
              />
            </div>
            <div className="col-7 p-0">
              <span className="bold-name">{playSong.name}</span> <br />
              <span className="text-muted light-name">{playSong.name}</span>
            </div>
            <div className="col-1 text-secondary song-fav">
              <i className="far fa-heart"></i>
            </div>
            <div
              className="col-1 text-secondary song-info cursor-pointer"
              id="info"
            >
              <i className="far fa-ellipsis-h" id="show-info"></i>
            </div>
          </div>
          <div
            className="row border-top song-additional-info"
            id="additional-info"
          >
            <div className="col-12 padding-song-info" id="playlist">
              <div className="row">
                <div className="col-2 text-center">
                  <svg
                    width="19"
                    height="19"
                    viewBox="0 0 24 24"
                    data-type="addtopp"
                    title="Add to Playlist"
                  >
                    <g fill="none" fill-rule="evenodd">
                      <path d="M0 0h24v24H0z" data-type="addtopp"></path>
                      <path
                        className="icon-color"
                        data-type="addtopp"
                        d="M21 22H7a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1zm-1.5-2a.5.5 0 0 0 .5-.5v-11a.5.5 0 0 0-.5-.5h-11a.5.5 0 0 0-.5.5v11a.5.5 0 0 0 .5.5h11zm-5-2h-1a.5.5 0 0 1-.5-.5V15h-2.5a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5H13v-2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5V13h2.5a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H15v2.5a.5.5 0 0 1-.5.5zM4 5v14.778a2 2 0 0 1-2-2V3a1 1 0 0 1 1-1h14.778a2 2 0 0 1 2 2H5a1 1 0 0 0-1 1z"
                      ></path>
                    </g>
                  </svg>
                </div>
                <div className="col-10 p-0">Add to Playlist</div>
              </div>
            </div>
            <div className="col-12 padding-song-info" id="share">
              <div className="row">
                <div className="col-2 text-center">
                  <svg
                    width="15"
                    height="15"
                    viewBox="0 0 20 20"
                    data-type="sharep"
                  >
                    <path
                      className="icon-color"
                      data-type="sharep"
                      fill-rule="evenodd"
                      d="M16 18a2 2 0 1 1 .001-4.001A2 2 0 0 1 16 18M4 12a2 2 0 1 1 .001-4.001A2 2 0 0 1 4 12M16 2a2 2 0 1 1-.001 4.001A2 2 0 0 1 16 2m0 10c-1.2 0-2.266.542-3 1.382l-5.091-2.546c.058-.27.091-.549.091-.836 0-.287-.033-.566-.091-.836L13 6.618C13.734 7.458 14.8 8 16 8c2.206 0 4-1.794 4-4s-1.794-4-4-4-4 1.794-4 4c0 .287.033.566.091.836L7 7.382A3.975 3.975 0 0 0 4 6c-2.206 0-4 1.794-4 4s1.794 4 4 4c1.2 0 2.266-.542 3-1.382l5.091 2.546c-.058.27-.091.549-.091.836 0 2.206 1.794 4 4 4s4-1.794 4-4-1.794-4-4-4"
                    ></path>
                  </svg>
                </div>
                <div className="col-10 p-0">Share</div>
              </div>
            </div>
            <div className="col-12 padding-song-info" id="download">
              <div className="row">
                <div className="col-2 text-center">
                  <svg
                    width="14"
                    height="18"
                    viewBox="0 0 16 20"
                    data-type="downloadp"
                    data-value="downloadp"
                  >
                    <g fill="none" fill-rule="evenodd">
                      <path
                        data-type="downloadp"
                        data-value="downloadp"
                        d="M-4-1.995h24v24H-4z"
                      ></path>
                      <path
                        className="icon-color"
                        data-type="downloadp"
                        data-value="downloadp"
                        d="M14 16.001v2H2v-2H0v2c0 1.102.896 2 2 2h12c1.104 0 2-.898 2-2v-2h-2zM7.293 13.708a.997.997 0 0 0 1.414 0l5-5A1 1 0 0 0 13 7.001h-2v-6a1 1 0 0 0-1-1H6a1 1 0 0 0-1 1v6H3a1 1 0 0 0-.707 1.707l5 5zM6 9.001a1 1 0 0 0 1-1v-6h2v6a1 1 0 0 0 1 1h.586L8 11.587 5.414 9.001H6z"
                      ></path>
                    </g>
                  </svg>
                </div>
                <div className="col-10 p-0">Download</div>
              </div>
            </div>
            <div className="col-12 padding-song-info" id="similar-songs">
              <div className="row">
                <div className="col-2 text-center">
                  <svg width="16" height="15" viewBox="0 0 21 20">
                    <g
                      fill="none"
                      fill-rule="evenodd"
                      transform="translate(-1 -2)"
                    >
                      <path d="M0 0h24v24H0z"></path>
                      <path
                        className="icon-color"
                        d="M20 8H6.922l7.557-4.122-.958-1.756-10.834 5.91A1.998 1.998 0 0 0 1 10v10c0 1.103.897 2 2 2h17c1.103 0 2-.897 2-2V10c0-1.104-.897-2-2-2zM3 20V10h17l.001 10H3z"
                      ></path>
                      <path
                        className="icon-color"
                        d="M5 12h8v2H5zM5 16h6v2H5z"
                      ></path>
                      <circle
                        cx="16"
                        cy="16"
                        r="2"
                        className="icon-color"
                      ></circle>
                    </g>
                  </svg>
                </div>
                <div className="col-10 p-0">Play similar song</div>
              </div>
            </div>
            <div className="col-12 padding-song-info" id="info-song">
              <div className="row">
                <div className="col-2 text-center">
                  <svg
                    width="20"
                    height="20"
                    viewBox="0 0 26 26"
                    data-type="getsonginfofromque"
                  >
                    <path
                      className="icon-color"
                      title="Get Song Info"
                      data-type="getsonginfofromque"
                      fill-rule="evenodd"
                      d="M1 13C1 6.372 6.372 1 13 1s12 5.372 12 12-5.372 12-12 12S1 19.628 1 13zm2 0c0 5.524 4.477 10 10 10 5.524 0 10-4.477 10-10 0-5.524-4.477-10-10-10C7.476 3 3 7.477 3 13zm10.946 4.604h-1.778v-6.762h1.778v6.762zm-.896-7.49A1.06 1.06 0 0 1 12 9.05c0-.588.476-1.05 1.05-1.05.588 0 1.064.462 1.064 1.05 0 .588-.476 1.064-1.064 1.064z"
                    ></path>
                  </svg>
                </div>
                <div className="col-10 p-0">Get Song Info</div>
              </div>
            </div>
            <div className="col-12 padding-song-info" id="lyrics">
              <div className="row">
                <div className="col-2 text-center">
                  <svg
                    width="20px"
                    height="20px"
                    viewBox="0 0 20 20"
                    version="1.1"
                  >
                    <defs></defs>
                    <g
                      id="v2"
                      stroke="none"
                      stroke-width="1"
                      fill="none"
                      fill-rule="evenodd"
                    >
                      <g
                        id="player-states"
                        transform="translate(-20.000000, -1609.000000)"
                      >
                        <g
                          id="3-dots-Click"
                          transform="translate(0.000000, 1314.000000)"
                        >
                          <g
                            id="More---Song"
                            transform="translate(0.000000, 54.000000)"
                          >
                            <g id="More">
                              <g
                                id="Group-6-Copy-4"
                                transform="translate(20.000000, 241.000000)"
                              >
                                <g id="info">
                                  <polygon
                                    id="Shape"
                                    points="0 0 20 0 20 20 0 20"
                                  ></polygon>
                                  <g
                                    id="Group-3"
                                    transform="translate(2.000000, 4.000000)"
                                    fill-rule="nonzero"
                                  >
                                    <path
                                      class="icon-color"
                                      d="M5.99999471,9.99430567 C5.99999824,9.99620337 6,9.99810148 6,10 C6,11.6568542 4.65685425,13 3,13 C1.34314575,13 0,11.6568542 0,10 C0,8.34314575 1.34314575,7 3,7 C3.35063542,7 3.68722107,7.06015422 4,7.17070571 L4,0 L10.5004744,0 L10.5004744,2 L6,2 L6,9.99430567 Z M3,11 C3.55228475,11 4,10.5522847 4,10 C4,9.44771525 3.55228475,9 3,9 C2.44771525,9 2,9.44771525 2,10 C2,10.5522847 2.44771525,11 3,11 Z M8.43978176,6 L8.43978176,4 L14.8964428,4 L14.8964428,6 L8.43978176,6 Z M8.43978176,11 L8.43978176,9 L12.8964428,9 L12.8964428,11 L8.43978176,11 Z"
                                      id="Combined-Shape"
                                    ></path>
                                  </g>
                                </g>
                              </g>
                            </g>
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
                </div>
                <div className="col-10 p-0">View Lyrics</div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-9 border song-play p-0">
          <div className="row p-0 m-0">
            <div className="col-12 p-0 m-0 progress-bar">
              <progress
                id="seekbar"
                value="0"
                max="1"
                onClick={this.seek}
              ></progress>
            </div>
          </div>
          <div className="row px-3 pb-1">
            <div className="col-2" style={{ maxWidth: "150px" }}>
              <span className="prev-track cursor-pointer">
                <i
                  className="fas fa-step-backward"
                  onClick={() => handleChangeSong(-1)}
                ></i>
              </span>
              <span className="play-button cursor-pointer">
                {audio.paused ? (
                  <i
                    className="fas fa-play-circle"
                    onClick={() => this.handleSongPlay(audio)}
                  ></i>
                ) : (
                  <i
                    className="fas fa-pause-circle"
                    onClick={() => this.handleSongPause(audio)}
                  ></i>
                )}
              </span>
              <span className="next-track cursor-pointer">
                <i
                  className="fas fa-step-forward"
                  onClick={() => handleChangeSong(1)}
                ></i>
              </span>
            </div>
            <div className="col-2 p-0" style={{ maxWidth: "100px" }}>
              <span className="song-time">
                <span id="current-time"></span> / <span id="duration"></span>{" "}
              </span>
            </div>
            <div className="col-2" style={{ maxWidth: "140px" }}>
              <span className="volume-button cursor-pointer">
                <i class="fas fa-volume-down"></i>
              </span>
              <span className="shuffle-icon cursor-pointer">
                <svg
                  width="24px"
                  height="24px"
                  viewBox="0 0 24 24"
                  version="1.1"
                >
                  <defs></defs>
                  <g
                    id="Navigation---v2"
                    stroke="none"
                    stroke-width="1"
                    fill-rule="evenodd"
                  >
                    <g
                      id="player-shuffle"
                      transform="translate(-1355.000000, -278.000000)"
                    >
                      <g
                        id="Now-Playing---Track"
                        transform="translate(0.000000, 199.000000)"
                      >
                        <g
                          id="component/darkui/player-v2-copy-4"
                          transform="translate(0.000000, 54.000000)"
                        >
                          <g id="Player---v2">
                            <g
                              id="More-Controls"
                              transform="translate(1324.000000, 25.000000)"
                            >
                              <g
                                id="icon/shuffle"
                                transform="translate(31.000000, 0.000000)"
                              >
                                <path
                                  d="M10.5937499,9.40725781 L9.18749973,10.768145 L4,5.747984 L5.40625013,4.38709677 L10.5937499,9.40725781 Z M14.5,4.38709677 L20,4.38709677 L20,9.70967742 L18,7.77419355 L5.40625013,19.8709677 L4,18.5100805 L16.5,6.32258065 L14.5,4.38709677 Z M14.8125003,13.4899195 L18,16.3931453 L20,14.4576614 L20,19.7802421 L14.5,19.7802421 L16.5,17.8447582 L13.4062501,14.8508067 L14.8125003,13.4899195 Z"
                                  id="Shape"
                                ></path>
                              </g>
                            </g>
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                </svg>
              </span>
              <span className="repeat-icon cursor-pointer">
                <svg
                  width="24px"
                  height="24px"
                  viewBox="0 0 24 24"
                  version="1.1"
                >
                  <defs></defs>
                  <g
                    id="Navigation---v2"
                    stroke="none"
                    stroke-width="1"
                    fill-rule="evenodd"
                  >
                    <g
                      id="player-repeat"
                      transform="translate(-1391.000000, -278.000000)"
                    >
                      <g
                        id="Now-Playing---Track"
                        transform="translate(0.000000, 199.000000)"
                      >
                        <g
                          id="component/darkui/player-v2-copy-4"
                          transform="translate(0.000000, 54.000000)"
                        >
                          <g id="Player---v2">
                            <g
                              id="More-Controls"
                              transform="translate(1324.000000, 25.000000)"
                            >
                              <g
                                id="icon/repeat"
                                transform="translate(71.000000, 4.000000)"
                              >
                                <path
                                  d="M0,8.12903226 C0,6.37499974 0.567708267,4.80745961 1.7031248,3.42641135 C2.83854187,2.0453631 4.27083307,1.13306426 6,0.689516387 L6,2.62500026 C4.83333333,3.06854813 3.875,3.78931458 3.125,4.78729858 C2.375,5.78528206 2,6.89919381 2,8.12903226 C2,9.78225806 2.6041664,11.1330643 3.81250027,12.1814514 L6,10.0645161 L6,15.8709677 L0,15.8709677 L2.40625013,13.5423386 C0.8020832,12.1108872 0,10.3064516 0,8.12903226 L0,8.12903226 Z M16,0.387096774 L13.5937499,2.80645161 C15.1979168,4.25806452 16,6.06250013 16,8.21975794 C16,9.99395148 15.4322917,11.5715726 14.2968752,12.9526209 C13.1614581,14.3336692 11.7291669,15.2459675 10,15.6895164 L10,13.6330643 C11.1666667,13.1895164 12.125,12.4687499 12.875,11.4707659 C13.625,10.4727825 14,9.35887071 14,8.12903226 C14,6.47580645 13.3958336,5.12500026 12.1874997,4.07661316 L10,6.19354839 L10,0.387096774 L16,0.387096774 Z"
                                  id="Shape"
                                ></path>
                              </g>
                            </g>
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                </svg>
              </span>
            </div>
            <div
              className="col-1 right-border"
              style={{ maxWidth: "1px" }}
            ></div>
            <div className="col-1 text-center p-0 m-0">
              <button
                className="btn quality-button border px-2 btn-block m-0 py-1"
                id="quality"
              >
                {quality} <i className="fas fa-caret-up" id="caret"></i>
              </button>
              <div className="quality-box shadow row px-0" id="quality-box">
                <div className="col-12 py-1 mt-3 px-4 cursor-pointer text-dark">
                  SOUND QUALITY
                </div>

                <div
                  className={
                    "col-12 py-3 px-4 cursor-pointer border-bottom " +
                    this.handleCurrentQuality("HD")
                  }
                  id="HD"
                >
                  <Link
                    to="/https://gaana.com/subscribe/buy-gaana-plus"
                    target="_blank"
                    className="text-decoration-none p-0"
                    style={{ color: "rgba(0, 0, 0, 0.5)" }}
                  >
                    <div className="row">
                      <div className="col-6" id="hdText">
                        HD
                      </div>
                      <div className="col-6 quality-icon">
                        {quality === "HD" && (
                          <i className="fas fa-check-circle"></i>
                        )}
                      </div>
                    </div>
                  </Link>
                </div>

                <div
                  className={
                    "col-12 py-3 px-4 cursor-pointer border-bottom " +
                    this.handleCurrentQuality("HIGH")
                  }
                  id="HIGH"
                  onClick={() => this.handleQuality("HIGH")}
                >
                  <div className="row">
                    <div className="col-6">High</div>
                    <div className="col-6 quality-icon">
                      {quality === "HIGH" && (
                        <i className="fas fa-check-circle"></i>
                      )}
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "col-12 py-3 px-4 cursor-pointer border-bottom " +
                    this.handleCurrentQuality("MEDIUM")
                  }
                  id="MEDIUM"
                  onClick={() => this.handleQuality("MEDIUM")}
                >
                  <div className="row">
                    <div className="col-6">Medium</div>
                    <div className="col-6 quality-icon">
                      {quality === "MEDIUM" && (
                        <i className="fas fa-check-circle"></i>
                      )}
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "col-12 py-3 px-4 cursor-pointer " +
                    this.handleCurrentQuality("LOW")
                  }
                  id="LOW"
                  onClick={() => this.handleQuality("LOW")}
                >
                  <div className="row">
                    <div className="col-6">Low</div>
                    <div className="col-6 quality-icon">
                      {quality === "LOW" && (
                        <i className="fas fa-check-circle"></i>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              className="col-1 right-border"
              style={{ maxWidth: "1px", marginLeft: "20px" }}
            ></div>
            <div className="col-2 p-0">
              <span className="autoplay" id="autoplay">
                AUTOPLAY
              </span>
              <button className="btn quality-button border px-3 py-1 text-dark">
                ON
              </button>
              <div className="aq-tooltip" id="tooltip">
                Keep it ON to automatically play tracks similar to your current
                Queue
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default PlaySong;
