import React, { Component } from "react";
import httpService from "./httpService";
import PlaySong from "./playSong";
import "./navbar.css";
import "./login.css";
import $ from "jquery";
import Login from "./login";
class Home extends Component {
  state = {
    pickCharts: [],
    topCharts: [],
    chartsIndex: 0,
    songs: [],
    songsIndex: 0,
    playSong: {},
    view: 0,
    progress: 0,
  };
  async fetchData() {
    $("#trending3").hide();
    $(document).ready(function () {
      $("#playList1").hide();
      $("#pickChart1").hover(function () {
        $("#playList1").toggle();
      });
      $("#playList2").hide();
      $("#pickChart2").hover(function () {
        $("#playList2").toggle();
      });
      $("#playList2_4").hide();
      $("#pickChart2_4").hover(function () {
        $("#playList2_4").toggle();
      });
      $("#playList3_4").hide();
      $("#pickChart3_4").hover(function () {
        $("#playList3_4").toggle();
      });
      $("#trending1").hide();
      $("#trendingSongs1").hover(function () {
        $("#trending1").toggle();
      });
      $("#trending2").hide();
      $("#trendingSongs2").hover(function () {
        $("#trending2").toggle();
      });

      $("#trendingSongs3").hover(function () {
        $("#trending3").toggle();
      });
      $("#trending4").hide();
      $("#trendingSongs4").hover(function () {
        $("#trending4").toggle();
      });
      $("#trending5").hide();
      $("#trendingSongs5").hover(function () {
        $("#trending5").toggle();
      });
      $("#trending6").hide();
      $("#trendingSongs6").hover(function () {
        $("#trending6").toggle();
      });
      $("#top1").hide();
      $("#topCharts1").hover(function () {
        $("#top1").toggle();
      });
      $("#top2").hide();
      $("#topCharts2").hover(function () {
        $("#top2").toggle();
      });
      $("#top3").hide();
      $("#topCharts3").hover(function () {
        $("#top3").toggle();
      });
      $("#top4").hide();
      $("#topCharts4").hover(function () {
        $("#top4").toggle();
      });
      $("#top5").hide();
      $("#topCharts5").hover(function () {
        $("#top5").toggle();
      });
      $("#top6").hide();
      $("#topCharts6").hover(function () {
        $("#top6").toggle();
      });
      $("#home").on("click", function () {
        $("#additional-info").hide();
        $("#show-info").addClass("fa-ellipsis-h");
        $("#show-info").removeClass("fa-chevron-down");
      });
    });
    let obj = this;
    let response = await httpService.get("/pickCharts");
    let songs = await httpService.get("/allSongs");
    let topCharts = await httpService.get("/topCharts");
    let audio = document.getElementById("myAudio");
    $("#myAudio").on("timeupdate", function () {
      if (this.currentTime === this.duration) {
        let index = songs.data.findIndex((sg) => sg.url === audio.src);
        audio.src = songs.data[index + 1].url;
        audio.play();
        let playSong = songs.data.find((sg) => sg.url === audio.src);
        obj.setState({ playSong: playSong });
      }
    });
    this.setState({
      pickCharts: response.data,
      songs: songs.data,
      topCharts: topCharts.data,
    });
  }
  componentDidMount() {
    this.fetchData();
  }
  handleChangeSong = (incr) => {
    let audio = document.getElementById("myAudio");
    let s1 = { ...this.state };
    let index = s1.songs.findIndex((sg) => sg.url === audio.src);
    s1.playSong = s1.songs[index + incr];
    audio.src = s1.songs[index + incr].url;
    audio.play();
    this.setState(s1);
  };
  handleleftSlideHover = (index) => (index === 0 ? "removehover1" : "");
  handleleftChevronHoverTopCharts = (view) =>
    view === 0 ? "removehover1" : "";
  handlerightSlideHover = (index) => (index === 4 ? "removehover1" : "");
  handleleftChevronHover = (index) => (index === 0 ? "removehover1" : "");
  handlerightChevronHover = (index) => (index === 40 ? "removehover1" : "");
  handlePickCharts = (incr) => {
    let s1 = { ...this.state };
    s1.chartsIndex = s1.chartsIndex + incr;
    this.setState(s1);
  };
  handleTrendingSongs = (incr) => {
    let s1 = { ...this.state };
    s1.songsIndex = s1.songsIndex + incr;
    this.setState(s1);
  };
  handleTopChartsView = (view) => this.setState({ view: view });
  StartOrStop = (audioFile) => {
    var audio = document.getElementById("myAudio");
    if (!audio.src || audio.src !== audioFile) audio.src = audioFile;
    if (audio.paused == false) audio.pause();
    else if (audio.src !== audioFile && audio.paused === false) audio.play();
    else audio.play();
    audio.onvolumechange = 0.3;
    $("#myAudio").on("timeupdate", function () {
      $("#seekbar").attr("value", this.currentTime / this.duration);
      var duration = this.duration; //song is object of audio.  song= new Audio();
      var dsec = new Number();
      var dmin = new Number();
      dsec = Math.floor(duration);
      dmin = Math.floor(dsec / 60);
      dmin = dmin >= 10 ? dmin : "0" + dmin;
      dsec = Math.floor(dsec % 60);
      dsec = dsec >= 10 ? dsec : "0" + dsec;
      let str1 = `${dmin}:${dsec}`;
      $("#duration").text(str1);
      var currentTime = this.currentTime; //song is object of audio.  song= new Audio();
      var csec = new Number();
      var cmin = new Number();
      csec = Math.floor(currentTime);
      cmin = Math.floor(csec / 60);
      cmin = cmin >= 10 ? cmin : "0" + cmin;
      csec = Math.floor(csec % 60);
      csec = csec >= 10 ? csec : "0" + csec;
      let str2 = `${cmin}:${csec}`;
      $("#current-time").text(str2);
    });
    let s1 = { ...this.state };
    s1.playSong = s1.songs.find((sg) => sg.url === audio.src);
    this.setState(s1);
  };
  render() {
    let {
      pickCharts = [],
      chartsIndex,
      songs = [],
      songsIndex,
      view,
      topCharts = [],
      playSong = {},
    } = this.state;
    let copyCharts = [];
    if (chartsIndex <= 3) {
      pickCharts.map(
        (ct, index) =>
          index >= chartsIndex &&
          index <= chartsIndex + 2 &&
          copyCharts.push(ct)
      );
    } else {
      pickCharts.map(
        (ct, index) => index >= 3 && index <= 5 && copyCharts.push(ct)
      );
    }
    let copySongs = [];
    if (songsIndex <= 38) {
      songs.map(
        (sg, index) =>
          index >= songsIndex && index <= songsIndex + 5 && copySongs.push(sg)
      );
    } else {
      songs.map((sg, index) => index >= 38 && copySongs.push(sg));
    }
    console.log(copySongs);
    return (
      <div className="container-fluid bg-grey home">
        <audio id="myAudio"></audio>
        <div className="row" id="home">
          <div className="col-9 ">
            <div className="row bg-white py-3 px-3">
              <div className="col-7 offset-1 second-navbar">
                <div className="row">
                  <div className="col-9 py-2">
                    <input
                      type="text"
                      className="search px-2"
                      placeholder="Search for Songs, Artists, Playlists And More"
                    />
                  </div>
                  <div className="col-3 search-btn py-2">
                    <span className="mx-3">
                      <svg width="24" height="24" viewBox="0 0 25 20">
                        <path
                          fill="#FFF"
                          fill-rule="evenodd"
                          d="M69.5 34a6.5 6.5 0 0 1 6.5 6.5c0 1.61-.59 3.09-1.56 4.23l.27.27h.79l5 5-1.5 1.5-5-5v-.79l-.27-.27A6.516 6.516 0 0 1 69.5 47a6.5 6.5 0 1 1 0-13zm0 2C67 36 65 38 65 40.5s2 4.5 4.5 4.5 4.5-2 4.5-4.5-2-4.5-4.5-4.5z"
                          transform="translate(-59 -32)"
                        ></path>
                      </svg>
                    </span>
                    Search
                  </div>
                </div>
              </div>
              <div className="col-2 py-1 mx-0" style={{ maxWidth: "140px" }}>
                <div className="mx-2 trending text-center py-1">
                  trending 20
                </div>
              </div>
              <div className="col-2 text-muted py-1 download-app mx-0">
                <div className="row">
                  <div className="col-8 py-1 px-0">
                    <span>Download App</span>
                  </div>
                  <div className="col-2 android text-left px-2">
                    <i className="fab fa-apple fa-2x"></i>
                  </div>
                  <div className="col-2 android text-right">
                    <i className="fab fa-2x fa-android"></i>
                  </div>
                </div>
              </div>
            </div>
            <div className="row home-bg">
              <div className="col-11 offset-1 top-picks-label my-4">
                <span>TOP PICKS</span>
              </div>
              {chartsIndex <= 3 && (
                <div className="col-11 offset-1">
                  <div className="row">
                    <div
                      className="col-5"
                      style={{
                        maxWidth: "340px",
                        maxHeight: "200px",
                      }}
                    >
                      <span
                        className={
                          "singleleftslide " +
                          this.handleleftSlideHover(chartsIndex)
                        }
                        onClick={() =>
                          chartsIndex > 0 && this.handlePickCharts(-1)
                        }
                      >
                        <svg
                          version="1.1"
                          id="Layer_1"
                          x="0px"
                          y="0px"
                          viewBox="0 0 50 50"
                          style={{ enableBackground: "new 0 0 50 50" }}
                          space="preserve"
                        >
                          <g>
                            <g transform="translate(-1370.000000, -243.000000)">
                              <g transform="translate(19.000000, 174.000000)">
                                <g transform="translate(1351.000000, 69.000000)">
                                  <circle
                                    className="leftSlide"
                                    data-value="leftslideshowcase"
                                    data-count="20"
                                    cx="25"
                                    cy="25"
                                    r="25"
                                  ></circle>
                                  <g transform="translate(22.000000, 16.000000)">
                                    <polygon
                                      className="black"
                                      data-value="leftslideshowcase"
                                      data-count="20"
                                      points="4,0.6 5.4,2 -1.6,9 5.4,16 4,17.4 -4.4,9"
                                    ></polygon>
                                  </g>
                                </g>
                              </g>
                            </g>
                          </g>
                        </svg>
                      </span>
                      <span id="pickChart1">
                        <img
                          src={copyCharts.length !== 0 && copyCharts[0].img}
                          alt=""
                          className="charts-img w3-animate-right"
                        />
                        <span className="hover-icon" id="playList1">
                          <svg
                            width="24"
                            height="25"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </span>
                    </div>
                    <div
                      className="col-5 mx-4"
                      style={{ maxWidth: "340px", maxHeight: "200px" }}
                    >
                      <span id="pickChart2">
                        <img
                          src={copyCharts.length !== 0 && copyCharts[1].img}
                          alt=""
                          className="charts-img w3-animate-right"
                        />
                        <span className="hover-icon" id="playList2">
                          <svg
                            width="24"
                            height="25"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </span>
                    </div>
                    <div className="col-2" style={{ overflow: "hidden" }}>
                      <img
                        src={copyCharts.length !== 0 && copyCharts[2].img}
                        alt=""
                        className="charts-img"
                      />
                      <span
                        className={
                          "singlerightslide " +
                          this.handlerightSlideHover(chartsIndex)
                        }
                        onClick={() =>
                          chartsIndex < 4 && this.handlePickCharts(1)
                        }
                      >
                        <svg
                          version="1.1"
                          id="Layer_1"
                          x="0px"
                          y="0px"
                          viewBox="0 0 50 50"
                          style={{ enableBackground: "new 0 0 50 50" }}
                          space="preserve"
                        >
                          <g>
                            <g transform="translate(-1370.000000, -243.000000)">
                              <g transform="translate(19.000000, 174.000000)">
                                <g transform="translate(1351.000000, 69.000000)">
                                  <circle
                                    className="leftSlide"
                                    data-value="rightslideshowcase"
                                    data-count="20"
                                    cx="25"
                                    cy="25"
                                    r="25"
                                  ></circle>
                                  <g transform="translate(22.000000, 16.000000)">
                                    <polygon
                                      className="black"
                                      data-value="rightslideshowcase"
                                      data-count="20"
                                      points="2,17.4 0.6,16 7.6,9 0.6,2 2,0.6 10.4,9"
                                    ></polygon>
                                  </g>
                                </g>
                              </g>
                            </g>
                          </g>
                        </svg>
                      </span>
                    </div>
                  </div>
                </div>
              )}
              {chartsIndex === 4 && (
                <div className="col-11 offset-1">
                  <div className="row">
                    <div className="col-2" style={{ overflow: "hidden" }}>
                      <img
                        src={copyCharts.length !== 0 && copyCharts[0].img}
                        alt=""
                        className="charts-img"
                      />
                      <span
                        className={
                          "singleleftslide4 " +
                          this.handleleftSlideHover(chartsIndex)
                        }
                        onClick={() =>
                          chartsIndex > 0 && this.handlePickCharts(-1)
                        }
                      >
                        <svg
                          version="1.1"
                          id="Layer_1"
                          x="0px"
                          y="0px"
                          viewBox="0 0 50 50"
                          style={{ enableBackground: "new 0 0 50 50" }}
                          space="preserve"
                        >
                          <g>
                            <g transform="translate(-1370.000000, -243.000000)">
                              <g transform="translate(19.000000, 174.000000)">
                                <g transform="translate(1351.000000, 69.000000)">
                                  <circle
                                    className="leftSlide"
                                    data-value="leftslideshowcase4"
                                    data-count="20"
                                    cx="25"
                                    cy="25"
                                    r="25"
                                  ></circle>
                                  <g transform="translate(22.000000, 16.000000)">
                                    <polygon
                                      className="black"
                                      data-value="leftslideshowcase"
                                      data-count="20"
                                      points="4,0.6 5.4,2 -1.6,9 5.4,16 4,17.4 -4.4,9"
                                    ></polygon>
                                  </g>
                                </g>
                              </g>
                            </g>
                          </g>
                        </svg>
                      </span>
                    </div>
                    <div
                      className="col-5 mx-4"
                      style={{ maxWidth: "340px", maxHeight: "200px" }}
                    >
                      <span id="pickChart2_4">
                        <img
                          src={copyCharts.length !== 0 && copyCharts[1].img}
                          alt=""
                          className="charts-img w3-animate-right"
                        />
                        <span className="hover-icon" id="playList2_4">
                          <svg
                            width="24"
                            height="25"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </span>
                    </div>
                    <div
                      className="col-5"
                      style={{
                        maxWidth: "340px",
                        maxHeight: "200px",
                      }}
                    >
                      <span id="pickChart3_4">
                        <img
                          src={copyCharts.length !== 0 && copyCharts[2].img}
                          alt=""
                          className="charts-img w3-animate-right"
                        />
                        <span className="hover-icon" id="playList3_4">
                          <svg
                            width="24"
                            height="25"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </span>
                      <span
                        className={
                          "singlerightslide " +
                          this.handlerightSlideHover(chartsIndex)
                        }
                      >
                        <svg
                          version="1.1"
                          id="Layer_1"
                          x="0px"
                          y="0px"
                          viewBox="0 0 50 50"
                          style={{ enableBackground: "new 0 0 50 50" }}
                          space="preserve"
                        >
                          <g>
                            <g transform="translate(-1370.000000, -243.000000)">
                              <g transform="translate(19.000000, 174.000000)">
                                <g transform="translate(1351.000000, 69.000000)">
                                  <circle
                                    className="leftSlide"
                                    data-value="rightslideshowcase"
                                    data-count="20"
                                    cx="25"
                                    cy="25"
                                    r="25"
                                  ></circle>
                                  <g transform="translate(22.000000, 16.000000)">
                                    <polygon
                                      class="black"
                                      data-value="rightslideshowcase"
                                      data-count="20"
                                      points="2,17.4 0.6,16 7.6,9 0.6,2 2,0.6 10.4,9 					"
                                    ></polygon>
                                  </g>
                                </g>
                              </g>
                            </g>
                          </g>
                        </svg>
                      </span>
                    </div>
                  </div>
                </div>
              )}
              <div
                className="col-11 offset-1 mt-5"
                style={{ paddingRight: "60px", paddingLeft: "25px" }}
              >
                <div className="row p-0">
                  <div className="col-12 bg-border p-0">
                    {chartsIndex === 0 && (
                      <div className="w3-grey b-active"></div>
                    )}
                    {chartsIndex === 1 && (
                      <div
                        className="w3-grey b-active"
                        style={{
                          marginLeft: "20%",
                        }}
                      ></div>
                    )}
                    {chartsIndex === 2 && (
                      <div
                        className="w3-grey b-active"
                        style={{
                          marginLeft: "40%",
                        }}
                      ></div>
                    )}
                    {chartsIndex === 3 && (
                      <div
                        className="w3-grey b-active"
                        style={{
                          marginLeft: "60%",
                        }}
                      ></div>
                    )}
                    {chartsIndex === 4 && (
                      <div
                        className="w3-grey b-active"
                        style={{
                          marginLeft: "80%",
                        }}
                      ></div>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-6 offset-1 top-picks-label my-4">
                <span>TRENDING SONGS</span>
              </div>
              <div className="col-5 view-label my-4">
                <span>View all</span>
                <span className="mx-2">
                  <i className="fa fa-chevron-right"></i>{" "}
                </span>
              </div>
              {songsIndex < 40 ? (
                <React.Fragment>
                  <div className="col-1 text-right chevron-left">
                    <span
                      className={
                        "singleleftslide cursor-pointer " +
                        this.handleleftChevronHover(songsIndex)
                      }
                      onClick={() =>
                        songsIndex > 0 && this.handleTrendingSongs(-5)
                      }
                    >
                      <svg
                        width="16"
                        height="18"
                        viewBox="0 0 16 18"
                        data-value="leftslide"
                        data-count="2"
                        data-type="trendingsong"
                      >
                        <g class="trending-song-slide" fill-rule="evenodd">
                          <path d="M14 17.414L15.414 16l-7-7 7-7L14 .586 5.586 9z"></path>
                          <path d="M9 17.414L10.414 16l-7-7 7-7L9 .586.586 9z"></path>
                        </g>
                      </svg>
                    </span>
                  </div>
                  <div className="col-11">
                    <div className="row">
                      <div
                        className="col-2"
                        id="trendingSongs1"
                        style={{ position: "relative" }}
                      >
                        <span>
                          <img
                            src={copySongs.length !== 0 && copySongs[0].img}
                            alt=""
                            className="songs-img"
                          />
                        </span>
                        <span
                          className="hover-icon-trending"
                          id="trending1"
                          onClick={() => this.StartOrStop(copySongs[0].url)}
                        >
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="trendingSongs2"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={copySongs.length !== 0 && copySongs[1].img}
                          alt=""
                          className="songs-img"
                        />
                        <span
                          className="hover-icon-trending"
                          id="trending2"
                          onClick={() => this.StartOrStop(copySongs[1].url)}
                        >
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="trendingSongs3"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={copySongs.length !== 0 && copySongs[2].img}
                          alt=""
                          className="songs-img"
                        />{" "}
                        <span
                          className="hover-icon-trending"
                          id="trending3"
                          onClick={() => this.StartOrStop(copySongs[2].url)}
                        >
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="trendingSongs4"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={copySongs.length !== 0 && copySongs[3].img}
                          alt=""
                          className="songs-img"
                        />
                        <span
                          className="hover-icon-trending"
                          id="trending4"
                          onClick={() => this.StartOrStop(copySongs[3].url)}
                        >
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="trendingSongs5"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={copySongs.length !== 0 && copySongs[4].img}
                          alt=""
                          className="songs-img"
                        />
                        <span
                          className="hover-icon-trending"
                          id="trending5"
                          onClick={() => this.StartOrStop(copySongs[4].url)}
                        >
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-1"
                        style={{
                          overflow: "hidden",
                          minWidth: "100px",
                          position: "relative",
                        }}
                        id="trendingSongs6"
                      >
                        <img
                          src={copySongs.length !== 0 && copySongs[5].img}
                          alt=""
                          className="songs-img"
                        />
                        <span
                          className="hover-icon-trending"
                          id="trending6"
                          onClick={() => this.StartOrStop(copySongs[5].url)}
                        >
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-1 text-right"
                        style={{ maxWidth: "40px" }}
                      >
                        <span
                          className={
                            "singlerightslideChevron cursor-pointer " +
                            this.handlerightChevronHover(songsIndex)
                          }
                          onClick={() =>
                            songsIndex < 40 && this.handleTrendingSongs(5)
                          }
                        >
                          <svg
                            width="16"
                            height="18"
                            viewBox="0 0 16 18"
                            data-value="rightslide"
                            data-count="2"
                            data-type="trendingsong"
                          >
                            <g
                              className="trending-song-slide"
                              fill-rule="evenodd"
                            >
                              <path d="M2 17.414L.586 16l7-7-7-7L2 .586 10.414 9z"></path>{" "}
                              <path d="M7 17.414L5.586 16l7-7-7-7L7 .586 15.414 9z"></path>{" "}
                            </g>
                          </svg>
                        </span>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <div className="col-1 text-right">
                    <span
                      className={
                        "singleleftslide cursor-pointer " +
                        this.handleleftChevronHover(songsIndex)
                      }
                      onClick={() =>
                        songsIndex > 0 && this.handleTrendingSongs(-5)
                      }
                    >
                      <svg
                        width="16"
                        height="18"
                        viewBox="0 0 16 18"
                        data-value="leftslide"
                        data-count="2"
                        data-type="trendingsong"
                      >
                        <g class="trending-song-slide" fill-rule="evenodd">
                          <path d="M14 17.414L15.414 16l-7-7 7-7L14 .586 5.586 9z"></path>
                          <path d="M9 17.414L10.414 16l-7-7 7-7L9 .586.586 9z"></path>
                        </g>
                      </svg>
                    </span>
                  </div>
                  <div className="col-11">
                    <div className="row">
                      <div
                        className="col-1"
                        style={{
                          overflow: "hidden",
                          minWidth: "100px",
                          position: "relative",
                        }}
                        id="trendingSongs1"
                      >
                        <img
                          src={copySongs.length !== 0 && copySongs[0].img}
                          alt=""
                          className="songs-img"
                        />
                        <span
                          className="hover-icon-trending"
                          id="trending1"
                          onClick={() => this.StartOrStop(copySongs[0].url)}
                        >
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="trendingSongs2"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={copySongs.length !== 0 && copySongs[1].img}
                          alt=""
                          className="songs-img"
                        />
                        <span
                          className="hover-icon-trending"
                          id="trending2"
                          onClick={() => this.StartOrStop(copySongs[1].url)}
                        >
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="trendingSongs3"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={copySongs.length !== 0 && copySongs[2].img}
                          alt=""
                          className="songs-img"
                        />
                        <span
                          className="hover-icon-trending"
                          id="trending3"
                          onClick={() => this.StartOrStop(copySongs[2].url)}
                        >
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="trendingSongs4"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={copySongs.length !== 0 && copySongs[3].img}
                          alt=""
                          className="songs-img"
                        />
                        <span
                          className="hover-icon-trending"
                          id="trending4"
                          onClick={() => this.StartOrStop(copySongs[3].url)}
                        >
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="trendingSongs5"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={copySongs.length !== 0 && copySongs[4].img}
                          alt=""
                          className="songs-img"
                        />
                        <span
                          className="hover-icon-trending"
                          id="trending5"
                          onClick={() => this.StartOrStop(copySongs[4].url)}
                        >
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2"
                        id="trendingSongs6"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={copySongs.length !== 0 && copySongs[5].img}
                          alt=""
                          className="songs-img"
                        />
                        <span
                          className="hover-icon-trending"
                          id="trending6"
                          onClick={() => this.StartOrStop(copySongs[5].url)}
                        >
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div className="col-1 pl-5" style={{ maxWidth: "100px" }}>
                        <span
                          className={
                            "singlerightslideChevron " +
                            this.handlerightChevronHover(songsIndex)
                          }
                        >
                          <svg
                            width="16"
                            height="18"
                            viewBox="0 0 16 18"
                            data-value="rightslide"
                            data-count="2"
                            data-type="trendingsong"
                          >
                            <g
                              className="trending-song-slide"
                              fill-rule="evenodd"
                            >
                              <path d="M2 17.414L.586 16l7-7-7-7L2 .586 10.414 9z"></path>{" "}
                              <path d="M7 17.414L5.586 16l7-7-7-7L7 .586 15.414 9z"></path>{" "}
                            </g>
                          </svg>
                        </span>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              )}
              <div
                className="col-11 offset-1 mt-5"
                style={{ paddingRight: "45px" }}
              >
                <hr />
              </div>
              <div className="col-6 offset-1 top-picks-label my-4">
                <span>TOP CHARTS</span>
              </div>
              <div className="col-5 view-label my-4">
                <span>View all</span>
                <span className="mx-2">
                  <i className="fa fa-chevron-right"></i>{" "}
                </span>
              </div>
              {view === 0 ? (
                <React.Fragment>
                  <div className="col-1 text-right chevron-left">
                    <span
                      className="singleleftslide removehover1 "
                      onClick={() => view > 0 && this.handleTopChartsView(0)}
                    >
                      <svg
                        width="16"
                        height="18"
                        viewBox="0 0 16 18"
                        data-value="leftslide"
                        data-count="2"
                        data-type="trendingsong"
                      >
                        <g class="trending-song-slide" fill-rule="evenodd">
                          <path d="M14 17.414L15.414 16l-7-7 7-7L14 .586 5.586 9z"></path>
                          <path d="M9 17.414L10.414 16l-7-7 7-7L9 .586.586 9z"></path>
                        </g>
                      </svg>
                    </span>
                  </div>
                  <div className="col-11">
                    <div className="row">
                      <div
                        className="col-2"
                        id="topCharts1"
                        style={{ position: "relative" }}
                      >
                        <span>
                          <img
                            src={topCharts.length !== 0 && topCharts[0].img}
                            alt=""
                            className="songs-img"
                          />
                        </span>
                        <span className="hover-icon-trending" id="top1">
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="topCharts2"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={topCharts.length !== 0 && topCharts[1].img}
                          alt=""
                          className="songs-img"
                        />
                        <span className="hover-icon-trending" id="top2">
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="topCharts3"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={topCharts.length !== 0 && topCharts[2].img}
                          alt=""
                          className="songs-img"
                        />{" "}
                        <span className="hover-icon-trending" id="top3">
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="topCharts4"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={topCharts.length !== 0 && topCharts[3].img}
                          alt=""
                          className="songs-img"
                        />
                        <span className="hover-icon-trending" id="top4">
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="topCharts5"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={topCharts.length !== 0 && topCharts[4].img}
                          alt=""
                          className="songs-img"
                        />
                        <span className="hover-icon-trending" id="top5">
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-1"
                        style={{
                          overflow: "hidden",
                          minWidth: "100px",
                          position: "relative",
                        }}
                        id="topCharts6"
                      >
                        <img
                          src={topCharts.length !== 0 && topCharts[5].img}
                          alt=""
                          className="songs-img"
                        />
                        <span className="hover-icon-trending" id="top6">
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-1 text-right text-danger"
                        style={{ maxWidth: "50px", position: "relative" }}
                      >
                        <span
                          className="top-charts-rightslide"
                          onClick={() =>
                            view < 1 && this.handleTopChartsView(1)
                          }
                        >
                          <svg
                            width="16"
                            height="18"
                            viewBox="0 0 16 18"
                            data-value="rightslide"
                            data-count="2"
                            data-type="trendingsong"
                          >
                            <g
                              className="trending-song-slide"
                              fill-rule="evenodd"
                            >
                              <path d="M2 17.414L.586 16l7-7-7-7L2 .586 10.414 9z"></path>{" "}
                              <path d="M7 17.414L5.586 16l7-7-7-7L7 .586 15.414 9z"></path>{" "}
                            </g>
                          </svg>
                        </span>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <div className="col-1 text-right chevron-left">
                    <span
                      className="singleleftslide "
                      onClick={() => view > 0 && this.handleTopChartsView(0)}
                    >
                      <svg
                        width="16"
                        height="18"
                        viewBox="0 0 16 18"
                        data-value="leftslide"
                        data-count="2"
                        data-type="trendingsong"
                      >
                        <g class="trending-song-slide" fill-rule="evenodd">
                          <path d="M14 17.414L15.414 16l-7-7 7-7L14 .586 5.586 9z"></path>
                          <path d="M9 17.414L10.414 16l-7-7 7-7L9 .586.586 9z"></path>
                        </g>
                      </svg>
                    </span>
                  </div>
                  <div className="col-11">
                    <div className="row">
                      <div
                        className="col-1"
                        id="topCharts1"
                        style={{
                          overflow: "hidden",
                          minWidth: "100px",
                          position: "relative",
                        }}
                      >
                        <span>
                          <img
                            src={topCharts.length !== 0 && topCharts[0].img}
                            alt=""
                            className="songs-img"
                          />
                        </span>
                        <span className="hover-icon-trending" id="top1">
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                        <audio
                          id="myAudio"
                          onClick={() => this.updateTrackTime()}
                        ></audio>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="topCharts2"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={topCharts.length !== 0 && topCharts[1].img}
                          alt=""
                          className="songs-img"
                        />
                        <span className="hover-icon-trending" id="top2">
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="topCharts3"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={topCharts.length !== 0 && topCharts[2].img}
                          alt=""
                          className="songs-img"
                        />{" "}
                        <span className="hover-icon-trending" id="top3">
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="topCharts4"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={topCharts.length !== 0 && topCharts[3].img}
                          alt=""
                          className="songs-img"
                        />
                        <span className="hover-icon-trending" id="top4">
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2 px-3"
                        id="topCharts5"
                        style={{ position: "relative" }}
                      >
                        <img
                          src={topCharts.length !== 0 && topCharts[4].img}
                          alt=""
                          className="songs-img"
                        />
                        <span className="hover-icon-trending" id="top5">
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-2"
                        style={{
                          position: "relative",
                        }}
                        id="topCharts6"
                      >
                        <img
                          src={topCharts.length !== 0 && topCharts[5].img}
                          alt=""
                          className="songs-img"
                        />
                        <span className="hover-icon-trending" id="top6">
                          <svg
                            width="20"
                            height="21"
                            viewBox="0 0 20 24"
                            className="playIcon"
                          >
                            <path
                              fill="#fff"
                              className="playIcon"
                              data-premium-value="0"
                              data-type="play"
                              data-value="playlist2580655"
                              fill-rule="evenodd"
                              d="M0 0v24l20-12z"
                            ></path>
                          </svg>
                        </span>
                      </div>
                      <div
                        className="col-1 text-right text-danger"
                        style={{ maxWidth: "50px", position: "relative" }}
                      >
                        <span className="top-charts-rightslide removehover1">
                          <svg
                            width="16"
                            height="18"
                            viewBox="0 0 16 18"
                            data-value="rightslide"
                            data-count="2"
                            data-type="trendingsong"
                          >
                            <g
                              className="trending-song-slide"
                              fill-rule="evenodd"
                            >
                              <path d="M2 17.414L.586 16l7-7-7-7L2 .586 10.414 9z"></path>{" "}
                              <path d="M7 17.414L5.586 16l7-7-7-7L7 .586 15.414 9z"></path>{" "}
                            </g>
                          </svg>
                        </span>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              )}
            </div>
          </div>
          <div className="col-3"></div>
        </div>
        {playSong.id && (
          <PlaySong
            playSong={playSong}
            handleChangeSong={this.handleChangeSong}
          />
        )}
      </div>
    );
  }
}
export default Home;
